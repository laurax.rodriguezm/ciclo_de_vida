package com.example.clico_de_vida

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var conteo: Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton= findViewById<Button>(R.id.botoncito)


        boton.setOnClickListener {

            val intent = Intent(this, CicloVidaActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onResume() {
        super.onResume()

        val texto= findViewById<TextView>(R.id.textView)
        conteo+= 1
        texto.setText("Visita Numero: " + conteo)

    }
}